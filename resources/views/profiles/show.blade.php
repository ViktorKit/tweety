<x-app>

    <header class="mb-6 relative">

        <div class="relative">

            <img src="/images/default-profile-banner.jpg" alt="profile-banner" class="mb-2">

            <img src="{{ $user->avatar }}" alt="user avatar"
                class="rounded-full mr-2 absolute bottom-0 left:50% transform -translate-x-1/2 translate-y-1/2"
                width="150" style="left: 50%">
        </div>


        <div class="flex justify-between items-center mb-6">
            <div style="max-width: 270px">
                <h3 class="font-bold text-2xl mb-">{{ $user->name }}</h3>
                <p class="text-sm">Joined {{ $user->created_at->diffForHumans() }}</p>
            </div>
            <div class="flex">

                @can('edit', $user)

                <a href="{{ $user->path('edit') }}"
                    class="rounded-full border border-gray-300 py-2 px-2 text-black text-xs mr-2">Edit
                    Profile
                </a>
                @endcan


                <x-follow-button :user="$user"></x-follow-button>
            </div>
        </div>

        <p class="text-sm">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque iusto facilis quae, magnam magni optio
            pariatur
            numquam culpa sapiente similique vel nulla ut ad error quam rerum autem? Illo, qui.
        </p>


    </header>

    @include('_timeline', [
    'tweets' => $tweets
    ])
</x-app>
